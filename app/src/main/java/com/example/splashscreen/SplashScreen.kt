package com.example.splashscreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

// Экран с SplashScreen'ом
class SplashScreen : AppCompatActivity() {

    // Функция, запускающаяся при создании экрана
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        // Задержка перед переходом на следующий экран (задержка в милисекундах, что означает, что
        // через две секунды будет переход)
        Handler().postDelayed({
            // Создание перехода (первый атрибут - откуда мы хотим сделать переход, второй - куда)
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            // Эта команда убирает возможность перейти на этот экран с помощью кнопки "назад"
            finish()
        }, 2500)
    }
}